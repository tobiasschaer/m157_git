# Editieren mit Markdown


## Was ist **Markdown**?

[Markdown](https://en.wikipedia.org/wiki/Markdown) ist eine simple Mark Language wie [HTML](https://en.wikipedia.org/wiki/HTML), welche zur Strukturierung von Texten verwendet wird.

So können zum Beispiel [Links](https://en.wikipedia.org/wiki/Hyperlink) in den Text eingebettet werden,
- Punkte
  - Einrückung

> Enschiebung.

Auch möglich sind Text verschönerungen wie: *kursiv*, **fett** oder ***beides***.

Um direkt einen Teil des Codes auf GitLab/GitHub zu zeigen, gibt es `Inlinecode`.

Hier ist noch  offizielle [Markdown Guide](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/) von GitLab zu finden.

![](images/cat.gif)

